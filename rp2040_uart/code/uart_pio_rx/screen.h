#ifndef SCREEN_H_
#define SCREEN_H_

#include <Arduino.h>

void displaySetup(void);
void displayPrint(String msg);

#endif 