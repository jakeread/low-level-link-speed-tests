#include "screen.h"
#include "uart_rx.pio.h"

// using an RP2040 XIAO 
// with earle philhower core 

// "D10" - GPIO 3 
#define PIN_DEBUG 3 

// on XIAO "RX" - GPIO 1
#define PIN_RX 1
#define PIO_BAUD 115200

// the PIO, and statemachine ? 
PIO pio = pio0;
uint sm = 0;
uint offset = 0;

// irq handler, 
void pio_irq_func(void){
  while(!pio_sm_is_rx_fifo_empty(pio, sm)){
    digitalWrite(PIN_DEBUG, !digitalRead(PIN_DEBUG));
    uint8_t data = uart_rx_program_getc(pio, sm);
  }
}


void setup(void){
  pinMode(PIN_LED_B, OUTPUT);
  digitalWrite(PIN_LED_B, LOW);

  pinMode(PIN_DEBUG, OUTPUT);
  digitalWrite(PIN_DEBUG, LOW);

  // the display setup 
  displaySetup();
  displayPrint("bonjour...");

  // add and init uart 
  offset = pio_add_program(pio, &uart_rx_program);
  uart_rx_program_init(pio, sm, offset, PIN_RX, PIO_BAUD);

  // setup the IRQ, 
  irq_add_shared_handler(PIO0_IRQ_0, pio_irq_func, PICO_SHARED_IRQ_HANDLER_DEFAULT_ORDER_PRIORITY);
  irq_set_enabled(PIO0_IRQ_0, true);
  // ???? lol ffs 
  const uint irq_index = PIO0_IRQ_0 - ((pio == pio0) ? PIO0_IRQ_0 : PIO1_IRQ_0); // Get index of the IRQ;
  // set PIO to interrupt when FIFO is NOT empty 
  pio_set_irqn_source_enabled(pio, irq_index, pis_sm0_rx_fifo_not_empty, true);
}

uint32_t lastUpdate = 0;
uint32_t updateInterval = 200;

void loop(void){
  // uint8_t data = uart_rx_program_getc(pio, sm);
  // digitalWrite(PIN_DEBUG, LOW);
  // ... 
  if(lastUpdate + updateInterval < millis()){
    lastUpdate = millis();
    digitalWrite(PIN_LED_B, !digitalRead(PIN_LED_B));
    // displayPrint(spipi_print());
    // displayPrint(String(rxCount) + "\n" + 
    //   String(rxSize) 
    // );
  }
}

