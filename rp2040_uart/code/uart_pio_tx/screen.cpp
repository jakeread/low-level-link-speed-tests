#include "screen.h"
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>

// OLED 
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define X_POS 0
#define Y_POS 0
#define TXT_SIZE 1

// even for displays with i.e. "0x78" printed on the back, 
// the address that works is 0x3C, IDK 
#define SCREEN_ADDRESS 0x3C

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT);

// warning: is blocking, takes ~ 33ms ! 
void displayPrint(String msg){
  display.clearDisplay();
  display.setCursor(X_POS, Y_POS);
  display.print(msg);
  display.display();
}

void displaySetup(void){
  // initialize the screen,
  // oddly, SWITCHCAPVCC is the option that works even though OLED is hooked to 5V 
  display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS);
  display.clearDisplay();
  display.display();
  display.setTextColor(SSD1306_WHITE);
  display.setTextSize(TXT_SIZE);
  display.setTextWrap(true);
  display.dim(false);
  displayPrint("bonjour...");
}