#include "screen.h"
#include "uart_tx.pio.h"

// using an RP2040 XIAO 
// with earle philhower core 

// "D10" - GPIO 3 
#define PIN_DEBUG 3 

// on XIAO "TX" - GPIO 0 
#define PIN_TX 0
#define PIO_BAUD 1000000

// the PIO, and statemachine ? 
PIO pio = pio0;
uint sm = 0;
uint offset = 0;

void setup(void){
  pinMode(PIN_LED_B, OUTPUT);
  digitalWrite(PIN_LED_B, LOW);

  pinMode(PIN_DEBUG, OUTPUT);
  digitalWrite(PIN_DEBUG, LOW);

  // the display setup 
  displaySetup();
  displayPrint("bonjour...");

  offset = pio_add_program(pio, &uart_tx_program);
  uart_tx_program_init(pio, sm, offset, PIN_TX, PIO_BAUD);
}

uint32_t lastUpdate = 0;
uint32_t updateInterval = 200;

uint8_t seqNum = 0;

void loop(void){
  digitalWrite(PIN_DEBUG, HIGH);
  // blocking tx-put: 
  uart_tx_program_putc(pio, sm, seqNum ++);
  delayMicroseconds(200);
  digitalWrite(PIN_DEBUG, LOW);
  // ... 
  if(lastUpdate + updateInterval < millis()){
    lastUpdate = millis();
    digitalWrite(PIN_LED_B, !digitalRead(PIN_LED_B));
    // displayPrint(spipi_print());
    // displayPrint(String(rxCount) + "\n" + 
    //   String(rxSize) 
    // );
  }
}

