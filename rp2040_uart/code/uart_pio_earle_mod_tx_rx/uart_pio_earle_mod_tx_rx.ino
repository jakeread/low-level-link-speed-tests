#include "screen.h"
#include "SerialModPIO.h"

// using an RP2040 XIAO 
// with earle philhower core 

// "D10" - GPIO 3 
#define PIN_DEBUG 3 

#define PIN_RX 1
#define PIN_TX 0 
#define PIO_BAUD 5000000

SerialModPIO serial(PIN_TX, PIN_RX);

void setup(void){
  pinMode(PIN_LED_B, OUTPUT);
  digitalWrite(PIN_LED_B, LOW);

  pinMode(PIN_DEBUG, OUTPUT);
  digitalWrite(PIN_DEBUG, LOW);

  // the display setup 
  displaySetup();
  displayPrint("bonjour...");

  serial.begin(PIO_BAUD);
}

uint32_t lastUpdate = 0;
uint32_t updateInterval = 1000;

uint8_t expectedRx = 0;
uint32_t missCount = 0;
uint32_t catchCount = 0;

uint8_t expected_miss = 0;
uint8_t actual_miss = 0;

uint8_t chars[256];

uint8_t seqNum = 0;

// and let's calculate actual bandwidth (success bytes / sec * 8) and bitrate ( * 10)
// and record avg per-cycle pickup ? 

uint32_t lastTx = 0;
uint32_t txInterval = 0;
float intervalEstimate = 0.0F;

uint32_t lastTransmit = 0;
uint32_t transmitInterval = 500;

void loop(void){

  // catch 'em AFAP 
  if(serial.available()){
    while(serial.available()){
      int data = serial.read();
      // earle core throws -1 if we have an error, 
      if(data < 0) {
        return;
      }
      // count total hits 
      catchCount ++;
      // stuff it 
      chars[catchCount & 255] = data;
      // check for missus 
      if(data != expectedRx){
        actual_miss = data;
        expected_miss = expectedRx;
        missCount ++;
      }
      expectedRx = data + 1;
    }
  }

  // and write AFAP, recording outgoing times 
  if(serial.availableForWrite()){
  // if(lastTransmit + transmitInterval < micros()){
    // lastTransmit = micros();
    digitalWrite(PIN_DEBUG, !digitalRead(PIN_DEBUG));
    serial.write(seqNum);
    seqNum += 1;
    uint32_t txTime = micros();
    txInterval = txTime - lastTx;
    lastTx = txTime;
    // intervalEstimate = (float)txInterval * 0.01F + intervalEstimate * 0.99F;
  }

  // ... 
  if(lastUpdate + updateInterval < millis()){
    lastUpdate = millis();
    digitalWrite(PIN_LED_B, !digitalRead(PIN_LED_B));
    displayPrint(String(missCount) + " / " + String(catchCount) + " \n" + 
    "miss: " + String((float)missCount / (float)catchCount, 9) + "\n" + 
    String(expected_miss) + ": " + String(actual_miss) + "\n" +
    "int: " + String(txInterval) + "\n" 
    // "avg interval: " + String(intervalEstimate, 4)
    // String(chars[0]) + ", " + String(chars[1]) + ", " + String(chars[2]) + ", " + String(chars[3]) + ", " + String(chars[4]) + ", " + String(chars[5]) + ", "
    );

    // displayPrint(spipi_print());
    // displayPrint(String(rxCount) + "\n" + 
    //   String(rxSize) 
    // );
  }
}

