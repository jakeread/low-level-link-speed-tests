#ifndef SPI_PI_H_
#define SPI_PI_H_

#include <Arduino.h>

void spipi_begin(void);
String spipi_print(void);

#endif 