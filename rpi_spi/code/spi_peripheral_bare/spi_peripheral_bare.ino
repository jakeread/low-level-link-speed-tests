#include "spipi.h"
#include "screen.h"

// using a (basically raspberry pi pico) W5500-EVB-Pico 
// with earle philhower core 

void setup(void){
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  // the display setup 
  displaySetup();

  // the spi setup 
  spipi_begin();
}

uint32_t lastUpdate = 0;
uint32_t updateInterval = 200;

void loop(void){
  if(lastUpdate + updateInterval < millis()){
    lastUpdate = millis();
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    displayPrint(spipi_print());
    // displayPrint(String(rxCount) + "\n" + 
    //   String(rxSize) 
    // );
  }
}