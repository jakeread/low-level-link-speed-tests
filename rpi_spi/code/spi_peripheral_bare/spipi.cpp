#include "spipi.h"
#include <hardware/spi.h>
#include <hardware/gpio.h>

// the relevant functions are here:
// /jaker/AppData/Local/Arduino15/packages/rp2040/3.6.1/pico-sdk/src/ ...
// /rp2040/rp2_common/hardware_spi/include/hardware/spi.h 

// the registers we're interested in here:
// /jaker/AppData/Local/Arduino15/packages/rp2040/3.6.1/pico-sdk/src/ ...
// /rp2040/hardware_structs/include/hardware/structs/spi.h

// for earle code reference, 
// /jaker/AppData/Local/Arduino15/packages/rp2040/3.6.1/libraries/ ...
// /SPISlave/src/SPISlave.cpp (etc) 

// for gpio, 
// /jaker/AppData/Local/Arduino15/packages/rp2040/3.6.1/pico-sdk/src/ ...
// /rp2040/rp2_common/hardware_gpio/include/hardware/gpio.h 

#define PIN_DEBUG 1 

#define PIN_CS    13
#define PIN_SCK   10 
#define PIN_RX    12 
#define PIN_TX    11 

#define BITRATE   11000000

#define SPI_INST  spi1 
#define SPI_HW spi_get_hw(SPI_INST)

#define SPI_IMSC_TXIM_ON  (1 << 3)
#define SPI_IMSC_RXIM_ON  (1 << 2)
#define SPI_IMSC_RTIM_ON  (1 << 1)
#define SPI_IMSC_RORIM_ON (1)

// some static buffs 
uint8_t rxBuffer[255];
volatile uint8_t rxPtr = 0;

String spipi_print(void){
  return String(rxBuffer[0]) + ", " + String(rxBuffer[1]);
}

uint8_t txBuffer[255];
volatile uint8_t txPtr = 0;
volatile uint8_t txLen = 64;

// we catch rising edges to delineate packet-end
void __not_in_flash_func(spipi_gpio_irq_handler)(uint gpio, uint32_t events){
  if(gpio_get(PIN_CS)){
    txPtr = 0;
    rxPtr = 0;
    // gpio_put(PIN_DEBUG, !gpio_get_out_level(PIN_DEBUG));
  }
}

void __not_in_flash_func(spipi_irq_handler)(void){
  // gpio_put(PIN_DEBUG, !gpio_get_out_level(PIN_DEBUG));
  // both have up to 8 bytes to read / write per interrupt ? 
  // write bytes while writable 
  for(uint8_t c = 0; c < 8; c ++){
    if(spi_is_writable(SPI_INST) && txPtr < txLen){
      SPI_HW->dr = txBuffer[txPtr];
      txPtr ++;
    }
  }
  // get bytes while readable, 
  for(uint8_t c = 0; c < 8; c ++){
    if(spi_is_readable(SPI_INST) && rxPtr < txLen){
      rxBuffer[rxPtr] = SPI_HW->dr;
      rxPtr ++;
    }
  }
}


void spipi_begin(void){
  // dummy pin 
  gpio_init(PIN_DEBUG);
  gpio_set_dir(PIN_DEBUG, GPIO_OUT);

  // dummy buffer 
  for(uint8_t i = 0; i < 255; i ++){
    txBuffer[i] = i;
  }

  // uh ?
  gpio_init(PIN_CS);
  gpio_set_dir(PIN_CS, GPIO_IN);
  
  // let's actually just get the CS LOW/HI interrupt first, 
  // so we can reset our buffer states (etc) 
  gpio_set_irq_enabled_with_callback(PIN_CS, GPIO_IRQ_EDGE_RISE, true, &spipi_gpio_irq_handler);
  // gpio_set_input_hysteresis_enabled(PIN_CS, false);

  // startup the peripheral ?
  spi_init(SPI_INST, BITRATE);
  spi_set_slave(SPI_INST, true);
  // spi_set_format(SPI_INST, 8, SPI_CPOL_0, SPI_CPHA_0, SPI_MSB_FIRST);
  // oddity where mode 0b11 allow continuous CS_LOW ? 
  // that's right, folks, we can only do this with 0b11 mode, insane: 
  // see https://github.com/raspberrypi/pico-sdk/issues/88#issuecomment-1402204730 
  // and see RP2040 datasheet at 4.4.3.13 ! 
  spi_set_format(SPI_INST, 8, SPI_CPOL_1, SPI_CPHA_1, SPI_MSB_FIRST);

  // assign pins to SPI peripheral, 
  // gpio_set_function(PIN_CS, GPIO_FUNC_SPI);
  gpio_set_function(PIN_SCK, GPIO_FUNC_SPI);
  gpio_set_function(PIN_RX, GPIO_FUNC_SPI);
  gpio_set_function(PIN_TX, GPIO_FUNC_SPI);

  // hook up our (one?) interrupt... 
  // NOTE: changing SPI_INST requires hard-coded change here 
  SPI_HW->imsc = SPI_IMSC_RXIM_ON | SPI_IMSC_TXIM_ON;
  irq_set_exclusive_handler(SPI1_IRQ, &spipi_irq_handler);
  irq_set_enabled(SPI1_IRQ, true);

  // preload the output ?
  for(uint8_t c = 0; c < 8; c ++){
    if(spi_is_writable(SPI_INST)){
      SPI_HW->dr = c;
    }
  }
}
