import spidev, time 
import numpy as np 
from plot_stamps import plot_stamps 

bitrate = 11000000

print(f'rate {bitrate/1e6}MBit/s bit period should be {1000000000/bitrate}ns')

spi = spidev.SpiDev()
spi.open(0, 0)
spi.max_speed_hz = bitrate 
spi.mode = 0b10

pck_len = 64
stamp_count = 10000
stamps = np.zeros(stamp_count)

test_pck = bytearray(pck_len)
for b in range(len(test_pck)):
  test_pck[b] = b + 1

print(test_pck)

bonked = False 
bonk_count = 0 

for i in range(stamp_count):
  # if bonked:
  #   break 
  ret = spi.xfer(test_pck)
  stamps[i] = time.perf_counter() * 1e6
  start = -1 
  for j in range(len(ret)):
    if start == -1:
      if ret[j] == 0:
        start = j 
        continue 
    else:
      if ret[j] != j - start:
        # print(f'BONK at {i} char {j} {start} {ret[j]}')
        # print(ret)
        bonk_count += 1 
        bonked = True 
        break 

spi.close() 

print(f"bonk {bonk_count} / {stamp_count}")

plot_stamps(stamps, stamp_count, pck_len, bonk_count / stamp_count)

# 1Mbit seems solid,
# 2.5Mbit and we have very occasional starvation 
# 5Mbit and we have at least one bad packet every run (1/1000?)
# 5Mbit with more F_CPU (250MHz) it is obviously improved (1/5000?)
# 10Mbit at 250MHz we are back to ~ (1/2000) starvations, 