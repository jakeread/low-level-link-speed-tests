import pandas as pd 
import matplotlib.pyplot as plt 

def plot_stamps(stamps, stamp_count, pck_len):
  # make df from stamps 
  df = pd.DataFrame({'timestamps': stamps})

  # calculate deltas between stamps 
  df['deltas'] = df['timestamps'].diff() 

  # clean NaN's 
  df = df.dropna()

  # wipe obviously-wrong deltas (i.e. the 1st, which goes 0-start-us) 
  df = df[df['deltas'] < 10000]

  # Plotting
  fig, ax1 = plt.subplots(figsize=(11, 3))

  ax1.set_xlim([50, 2000])

  # Primary x-axis (time deltas)
  df['deltas'].plot(kind='hist', bins=50, ax=ax1)
  ax1.set_xlabel('Time-Stamp Deltas (us) and equivalent (MBits/s)')
  ax1.set_ylabel(f'Frequency (of {stamp_count})')

  # get axis ticks to calculate equivalent bandwidths 
  x_ticks = ax1.get_xticks()
  ax1.set_xticks(x_ticks)
  bandwidths = [((pck_len * 8) * (1e6 / x)) / 1e6 for x in x_ticks]
  ticks = [] 

  for i in range(len(x_ticks)):
    print(i, x_ticks[i], bandwidths[i]) 
    ticks.append(f"{x_ticks[i]:.0f} ({bandwidths[i]:.3f})")

  ax1.set_xticklabels(ticks)

  plt.title(f'Single-Source COBS Data Sink Deltas, pck_len={pck_len}')

  plt.tight_layout()

  plt.show()