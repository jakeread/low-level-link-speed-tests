import socket, time, asyncio
from plot_stamps import plot_stamps
import numpy as np 


# Arduino's network settings
arduino_ip = '192.168.1.177'
arduino_port = 8888


# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


# bind the socket to our local addr and port, 
# 0.0.0.0 is a mask: listens on all interfaces (?) 
sock.bind(('0.0.0.0', 8888))


# test stats
stamp_count = 10000
stamps = np.zeros(stamp_count)
stamp_counter = 0 
pck_len = 64


def handle_pck(data):
  global stamps, stamp_counter, pck_len
  if (stamp_counter >= stamp_count):
    return 
  stamps[stamp_counter] = time.perf_counter() * 1e6
  stamp_counter += 1 
  if (stamp_counter >= stamp_count):
    plot_stamps(stamps, stamp_count, pck_len)


# let's setup an async receiver: 
async def receiver(socket, handler):
  while True:
    data = socket.recv(1024)
    handler(data)
    await asyncio.sleep(0)

async def main():

  task = asyncio.create_task(receiver(sock, handle_pck))

  await asyncio.gather(task)

asyncio.run(main())


# test data 
# out_pck = bytearray(pck_len)


# for i in range(stamp_count):
#   # Send a message
#   # message = b'Hello, Arduino!'
#   sock.sendto(out_pck, (arduino_ip, arduino_port))
#   # print(f'Transmitted...')

#   # Receive response
#   data, addr = sock.recvfrom(1024)
#   # print(f'Received message: {data}')

#   stamps[i] = time.perf_counter() * 1e6

# print('stamps', stamps)

# plot_stamps(stamps, stamp_count, pck_len) 