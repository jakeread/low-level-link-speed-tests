/*
  DHCP-based IP printer

  This sketch uses the DHCP extensions to the Ethernet library
  to get an IP address via DHCP and print the address obtained.
  using an Arduino WIZnet Ethernet shield.

  Circuit:
   Ethernet shield attached to pins 10, 11, 12, 13

  created 12 April 2011
  modified 9 Apr 2012
  by Tom Igoe
  modified 02 Sept 2015
  by Arturo Guadalupi

 */

// https://github.com/arduino-libraries/Ethernet/blob/master/examples/DhcpAddressPrinter/DhcpAddressPrinter.ino
// https://github.com/arduino-libraries/Ethernet/blob/master/examples/UDPSendReceiveString/UDPSendReceiveString.ino

#include <SPI.h>
#include <Ethernet.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Wire.h>

#define DEBUG_PIN 4

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define X_POS 0
#define Y_POS 0
#define TXT_SIZE 1

// even for displays with i.e. "0x78" printed on the back, 
// the address that works is 0x3C, IDK 
#define SCREEN_ADDRESS 0x3C

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire1);

// warning: is blocking, takes ~ 33ms ! 
void display_print(String msg){
  display.clearDisplay();
  display.setCursor(X_POS, Y_POS);
  display.print(msg);
  display.display();
}

// AFAIK we can just make this up for the time being,
// but should maybe accomodate proper MAC using a MAC IC or sth sth 
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

IPAddress ip(192, 168, 1, 177);
unsigned int localPort = 8888;

char packetBuffer[2048];   // what's the max, actually ? 
char replyBuffer[2048] = "ack!";

EthernetUDP EUDP;

void setup() {
  // want that LED,
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // a scope debug out 
  pinMode(DEBUG_PIN, OUTPUT);
  digitalWrite(DEBUG_PIN, LOW);

  // confirm our I2C pins are here ?
  Wire1.setSDA(2);
  Wire1.setSCL(3);

  // initialize the screen,
  // oddly, SWITCHCAPVCC is the option that works even though OLED is hooked to 5V 
  display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS);
  display.clearDisplay();
  display.display();
  display.setTextColor(SSD1306_WHITE);
  display.setTextSize(TXT_SIZE);
  display.setTextWrap(true);
  display.dim(false);
  display_print("bonjour...");

  // You can use Ethernet.init(pin) to configure the CS pin
  Ethernet.init(17);
  display_print("ethernet beginning...");
  
  // start the Ethernet connection:
  Ethernet.begin(mac, ip);

  // and the udp link 
  EUDP.begin(localPort);
}

uint32_t lastUpdate = 0;
uint32_t updateInterval = 500;

uint32_t rxCount = 0;
uint32_t txCount = 0;

IPAddress txIP(192, 168, 1, 178);
uint32_t txPort = 8888;

void loop() {
  // just tx, forever, see if we break flowcontrol 
  digitalWrite(DEBUG_PIN, HIGH);
  EUDP.beginPacket(txIP, txPort);
  EUDP.write(replyBuffer, 64);
  EUDP.endPacket();
  txCount ++;
  digitalWrite(DEBUG_PIN, LOW);

  // size_t len = EUDP.parsePacket();
  // if(len){
  //   digitalWrite(DEBUG_PIN, HIGH);
  //   // get the pck, 
  //   rxCount ++;
  //   // display_print("rx " + String(len));
  //   EUDP.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
  //   // reply to the pck, 
  //   EUDP.beginPacket(EUDP.remoteIP(), EUDP.remotePort());
  //   EUDP.write(replyBuffer, len);
  //   EUDP.endPacket();
  //   digitalWrite(DEBUG_PIN, LOW);
  // }
  
  // report status
  // TODO: use only one line of the disp for this, 
  // keep a string-thing somewhere, just update sections of it ? 
  // i.e. display.println(str1), /// etc... 
  if(lastUpdate + updateInterval < millis()){
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    lastUpdate = millis();
    // display_print("STAMP: " + String(lastUpdate));
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      display_print("Ethernet shield was not found.");
    } else if (Ethernet.linkStatus() == LinkOFF) {
      display_print("Link OFF");
    } else if (Ethernet.linkStatus() == LinkON){
      display_print("ON: " + (Ethernet.localIP().toString()) + "\n" + String(rxCount) + "\nTX: " + String(txCount));
    } else if (Ethernet.linkStatus() == Unknown){
      display_print("Link UNKNOWN");
    }
  }

  // maintain is required to renew DHCP leases, but we are static-ip'ing this mf'er I think 
  // see docs: https://www.arduino.cc/reference/en/libraries/ethernet/ethernet.maintain/ 
  // switch (Ethernet.maintain()) {
  //   case 1:
  //     //renewed fail
  //     display_print("Error: renewed fail");
  //     break;
  //   case 2:
  //     //renewed success
  //     display_print("Renew, IP: " + Ethernet.localIP());
  //     break;
  //   case 3:
  //     //rebind fail
  //     display_print("Error: rebind fail");
  //     break;
  //   case 4:
  //     //rebind success
  //     //print your local IP address:
  //     display_print("Rebind, IP: " + Ethernet.localIP());
  //     break;

  //   default:
  //     //nothing happened
  //     break;
  // }
}

// TODO: run the display from the second core, since 
// `Wire` uses blocking writes, for when we 
// are going hardo-perf-mode, 

// void setup1(){
// }

// void loop1(){
// }