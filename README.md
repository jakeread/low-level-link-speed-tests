# Low Level Link Speed Tests

For a more conversational write-up, see [the blog post.](https://ekswhyzee.com/2024/01/04/link-layer-speed-tests.html)

TL:DR; we want to find out actually how-fast links are between embedded systems and high-level systems, mostly for machine building, but useful across the board. In quick tests, I found that most links under-perform their apparent rates by more than half. 

### USB

![histo](usb_serial/images/2023-12-27_simple-01-baseline.png)
> RP2040, 0.45Mbit/s

![histo](usb_serial/images/2024-01-04_teensy-usb-32.png)
> Teensy 4.0, 20Mbit/s

### WizNet Ethernet 

![histo](wiznet_ethernet/images/2023-12-28_eth-oneway-64.png)

### SPI to PI

![histo](rpi_spi/images/2024-01-04_spi-histo-10Mb.png)

---

# TODO Here 

I am electing microcontrollers for the position of routers-et-cetera, and the RP2040 was a top candidate but has me spooked after these tests. I want to do some more testing:

## Better Tests

I want to use all of these links in the context of my link-agnostic transport layer, which currently uses shoddy ad-hoc links. I would like to have some generic wrapper code that would bundle questionable links (like these) and do i.e. error catching and test-suite-implementation in a more regular manner. It could presumably be built with a kind of simple HAL to put and to get packets.

## Testing other Systems 

- test the D51 for device-to-device uart 
- the D51 and RPi 5 for big-cpu-to-small-mcu SPI test redux 
- consider the Teensy for the router, that 600MHz and advertised-to-be-small ISR seems juicy 
- Teensy USB might be blazing fast to boot, Paul seems like the Real Deal (TM) and Teensy 4.0 page mentions 480Mbit/s USB... 

## Improving Existing Tests 

- the RP2040's PIO *aught* to be killer at this, so it is likely that I simply haven't really cracked it. for example, each has an 8-space wide FIFO, each of which is 32 bits long, this means one interrupt should be able to switch a 32-byte long packet (in SPI-mode) - UART (it looks like) can go to 16 bit words 
- we also haven't really implemented interrupts on the 2040 properly, far as I can tell: we should be able to get one latched whenever a fifo is *NOT FULL* rather than being half-full, etc, which would help prevent some chunking (earlier warning) 
 