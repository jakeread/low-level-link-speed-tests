from cobs_usb_serial import CobsUsbSerial 
from plot_stamps import plot_stamps
import struct 
import numpy as np 

ser = CobsUsbSerial("COM43") 

stamp_count = 1000
pck_len = 128

stamps = np.zeros(stamp_count)

for i in range(stamp_count):
  bts = ser.read()
  if len(bts) == pck_len:
    stamp = struct.unpack('=I', bts[:4])
    stamps[i] = stamp[0]

print("stamps, ", stamps)

plot_stamps(stamps, stamp_count, pck_len, 0)