from cobs_usb_serial import CobsUsbSerial 
import struct 
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 

ser_one = CobsUsbSerial("COM23") 
ser_two = CobsUsbSerial("COM31")
ser_three = CobsUsbSerial("COM33")
ser_four = CobsUsbSerial("COM35")

stamp_count = 1000
pck_len = 128

stamps_one = np.zeros(stamp_count)
counter_one = 0 

stamps_two = np.zeros(stamp_count)
counter_two = 0 

stamps_three = np.zeros(stamp_count)
counter_three = 0 

stamps_four = np.zeros(stamp_count)
counter_four = 0 

def cycle(ser, stamps, counter):
  if counter >= stamp_count:
    return counter 
  bts = ser.read()
  if bts:
    if len(bts) == pck_len:
      stamp = struct.unpack("=I", bts[:4])
      stamps[counter] = stamp[0]
      counter += 1
  return counter 

while True:
  counter_one = cycle(ser_one, stamps_one, counter_one)
  counter_two = cycle(ser_two, stamps_two, counter_two)
  counter_three = cycle(ser_three, stamps_three, counter_three)
  counter_four = cycle(ser_four, stamps_four, counter_four)

  if counter_one == stamp_count and counter_two == stamp_count and counter_three == stamp_count and counter_four == stamp_count:
    break 

# print("stamps, ", stamps_one, stamps_two)

def plot_stamps(stamps):
  # make df from stamps 
  df = pd.DataFrame({'timestamps': stamps})

  # calculate deltas between stamps 
  df['deltas'] = df['timestamps'].diff() 

  # clean NaN's 
  df = df.dropna()

  # wipe obviously-wrong deltas (i.e. the 1st, which goes 0-start-us) 
  df = df[df['deltas'] < 100000]

  # Plotting
  fig, ax1 = plt.subplots(figsize=(11, 3))

  ax1.set_xlim([1750, 4750])

  # Primary x-axis (time deltas)
  df['deltas'].plot(kind='hist', bins=100, ax=ax1)
  ax1.set_xlabel('Time-Stamp Deltas (us) and equivalent (MBits/s)')
  ax1.set_ylabel(f'Frequency (of {stamp_count})')

  # get axis ticks to calculate equivalent bandwidths 
  x_ticks = ax1.get_xticks()
  ax1.set_xticks(x_ticks)
  bandwidths = [((pck_len * 8) * (1e6 / x)) / 1e6 for x in x_ticks]
  ticks = [] 

  for i in range(len(x_ticks)):
    print(i, x_ticks[i], bandwidths[i]) 
    ticks.append(f"{x_ticks[i]:.0f} ({bandwidths[i]:.3f})")

  ax1.set_xticklabels(ticks)

  plt.title(f'Single-Source COBS Data Sink Deltas, pck_len={pck_len}')

  plt.tight_layout()

  plt.show()

plot_stamps(stamps_one)
plot_stamps(stamps_two)
plot_stamps(stamps_three)
plot_stamps(stamps_four)