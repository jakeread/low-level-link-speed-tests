from cobs import cobs
import serial


class CobsUsbSerial:
    def __init__(self, port, baudrate=115200):
        self.port = port
        self.ser = serial.Serial(port, baudrate=baudrate, timeout=1)
        self.buffer = bytearray()

    def write(self, data: bytes):
        data_enc = cobs.encode(data) + b"\x00"
        self.ser.write(data_enc)

    def read(self):
        byte = self.ser.read(1)
        if not byte:
            return 
        if byte == b"\x00":
            if len(self.buffer) > 0:
                data = cobs.decode(self.buffer)
                self.buffer = bytearray() 
                return data
            else: 
                return 
        else:
            self.buffer += byte 
        # data_enc = self.ser.read_until(b"\x00")
        # data = cobs.decode(data_enc[:-1])
        # return data
