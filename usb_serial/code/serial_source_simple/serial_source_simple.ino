// py-transport-tester 
// using the RP2040 at 133 MHz 

void setup() {
  Serial.begin(9600);
  pinMode(PIN_LED_B, OUTPUT);
  digitalWrite(PIN_LED_B, HIGH);
}

uint32_t lastBlink = 0;

void loop() {
  // write forever, 
  if(Serial.availableForWrite()){
    while(Serial.availableForWrite()){
      Serial.write(125);
    }
  }
  // blink to see hangups 
  if(lastBlink + 100 < millis()){
    lastBlink = millis();
    digitalWrite(PIN_LED_B, !digitalRead(PIN_LED_B));
  }
}
