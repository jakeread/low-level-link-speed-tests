#include "COBSUSBSerial.h"

// py-transport-tester 
// using the RP2040 at 200MHz 

#define MSG_GET_ID        7
#define MSG_ECHO_TEST     11 
#define MSG_SET_TARG_VEL  21
#define MSG_SET_TARG_POS  22
#define MSG_SET_POSITION  23 
#define MSG_SET_CURRENT   24
#define MSG_GET_STATES    25
#define MSG_ACK           31 
#define MSG_NACK          32

COBSUSBSerial cobs(&Serial);

void setup() {
  cobs.begin();
  pinMode(PIN_LED_R, OUTPUT);
  pinMode(PIN_LED_G, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);
  digitalWrite(PIN_LED_R, HIGH);
  digitalWrite(PIN_LED_G, HIGH);
  digitalWrite(PIN_LED_B, HIGH);
}

union chunk_uint32 {
  uint8_t bytes[4];
  uint32_t u;
};

chunk_uint32 chunk;

uint32_t lastBlink = 0;

void loop() {
  // run comms
  cobs.loop();
  // tx a stamp AFAP 
  if(cobs.clearToSend()){
    chunk.u = micros();
    cobs.send(chunk.bytes, 128);
    digitalWrite(PIN_LED_G, !digitalRead(PIN_LED_G));
  }
  // blink to see hangups 
  if(lastBlink + 100 < millis()){
    lastBlink = millis();
    digitalWrite(PIN_LED_B, !digitalRead(PIN_LED_B));
  }
}
