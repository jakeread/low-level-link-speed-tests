from cobs_usb_serial_async import CobsUsbSerial 
import asyncio 
import struct 
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 

stamp_count = 1000
pck_len = 128

stamps_one = np.zeros(stamp_count)
counter_one = 0 
plot_one = False 

stamps_two = np.zeros(stamp_count)
counter_two = 0 
plot_two = False 

stamps_three = np.zeros(stamp_count)
counter_three = 0 
plot_three = False 

stamps_four = np.zeros(stamp_count)
counter_four = 0 
plot_four = False 

def plot_stamps(stamps):
  # make df from stamps 
  df = pd.DataFrame({'timestamps': stamps})

  # calculate deltas between stamps 
  df['deltas'] = df['timestamps'].diff() 

  # clean NaN's 
  df = df.dropna()

  # wipe obviously-wrong deltas (i.e. the 1st, which goes 0-start-us) 
  df = df[df['deltas'] < 100000]

  # Plotting
  fig, ax1 = plt.subplots(figsize=(11, 3))

  ax1.set_xlim([1750, 4750])

  # Primary x-axis (time deltas)
  df['deltas'].plot(kind='hist', bins=100, ax=ax1)
  ax1.set_xlabel('Time-Stamp Deltas (us) and equivalent (MBits/s)')
  ax1.set_ylabel(f'Frequency (of {stamp_count})')

  # get axis ticks to calculate equivalent bandwidths 
  x_ticks = ax1.get_xticks()
  ax1.set_xticks(x_ticks)
  bandwidths = [((pck_len * 8) * (1e6 / x)) / 1e6 for x in x_ticks]
  ticks = [] 

  for i in range(len(x_ticks)):
    print(i, x_ticks[i], bandwidths[i]) 
    ticks.append(f"{x_ticks[i]:.0f} ({bandwidths[i]:.3f})")

  ax1.set_xticklabels(ticks)

  plt.title(f'Single-Source COBS Data Sink Deltas, pck_len={pck_len}')

  plt.tight_layout()

  plt.show()


def cycle(data, stamps, counter, plot):
  if counter >= stamp_count:
    if not plot:
      plot = True 
      plot_stamps(stamps)
    return counter, plot 
  if len(data) == pck_len:
    stamp = struct.unpack("=I", data[:4])
    stamps[counter] = stamp[0]
    counter += 1
  return counter, plot 


def port_one_handler(data):
  global counter_one, plot_one
  counter_one, plot_one = cycle(data, stamps_one, counter_one, plot_one)

def port_two_handler(data):
  global counter_two, plot_two
  counter_two, plot_two = cycle(data, stamps_two, counter_two, plot_two)

def port_three_handler(data):
  global counter_three, plot_three
  counter_three, plot_three = cycle(data, stamps_three, counter_three, plot_three)

def port_four_handler(data):
  global counter_four, plot_four
  counter_four, plot_four = cycle(data, stamps_four, counter_four, plot_four)


async def main():
  port_one = CobsUsbSerial("COM23") 
  port_two = CobsUsbSerial("COM31")
  port_three = CobsUsbSerial("COM33")
  port_four = CobsUsbSerial("COM36")

  task_one = asyncio.create_task(port_one.attach(port_one_handler))
  task_two = asyncio.create_task(port_two.attach(port_two_handler))
  task_three = asyncio.create_task(port_three.attach(port_three_handler))
  task_four = asyncio.create_task(port_four.attach(port_four_handler))

  await asyncio.gather(task_one, task_two, task_three, task_four)

asyncio.run(main())