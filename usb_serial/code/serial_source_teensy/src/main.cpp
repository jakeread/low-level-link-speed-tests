#include <Arduino.h>
#include "COBSUSBSerial.h"

#define PIN_LED 13 

COBSUSBSerial cobs(&Serial);

void setup() {
  cobs.begin();
  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, HIGH);
}

union chunk_uint32 {
  uint8_t bytes[4];
  uint32_t u;
};

chunk_uint32 chunk;

uint32_t lastBlink = 0;

void loop() {
  // run comms
  cobs.loop();
  // tx a stamp AFAP 
  if(cobs.clearToSend()){
    chunk.u = micros();
    cobs.send(chunk.bytes, 128);
  }
  // blink to see hangups 
  if(lastBlink + 100 < millis()){
    lastBlink = millis();
    digitalWrite(PIN_LED, !digitalRead(PIN_LED));
  }
}
