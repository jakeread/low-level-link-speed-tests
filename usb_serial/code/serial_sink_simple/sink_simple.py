import time, serial 
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 

class UsbSerial:
    def __init__(self, port, baudrate=115200):
        self.port = port
        self.ser = serial.Serial(port, baudrate=baudrate, timeout=1)

    def write(self, data: bytes):
        self.ser.write(data)

    def read(self):
        return self.ser.read()

ser = UsbSerial("COM23") 

stamp_count = 1000
pck_len = 32

stamps = np.zeros(stamp_count)

for i in range(stamp_count):
  count = 0 
  while True:
    if count >= 32:
      stamps[i] = time.perf_counter() * 1e6
      break 
    byte = ser.read()
    if byte:
      count += 1 

print("stamps, ", stamps)

def plot_stamps(stamps):
  # make df from stamps 
  df = pd.DataFrame({'timestamps': stamps})

  # calculate deltas between stamps 
  df['deltas'] = df['timestamps'].diff() 

  # clean NaN's 
  df = df.dropna()

  # wipe obviously-wrong deltas (i.e. the 1st, which goes 0-start-us) 
  df = df[df['deltas'] < 100000]

  # Plotting
  fig, ax1 = plt.subplots(figsize=(11, 3))

  ax1.set_xlim([250, 1100])

  # Primary x-axis (time deltas)
  df['deltas'].plot(kind='hist', bins=100, ax=ax1)
  ax1.set_xlabel('Time-Stamp Deltas (us) and equivalent (MBits/s)')
  ax1.set_ylabel(f'Frequency (of {stamp_count})')

  # get axis ticks to calculate equivalent bandwidths 
  x_ticks = ax1.get_xticks()
  ax1.set_xticks(x_ticks)
  bandwidths = [((pck_len * 8) * (1e6 / x)) / 1e6 for x in x_ticks]
  ticks = [] 

  for i in range(len(x_ticks)):
    print(i, x_ticks[i], bandwidths[i]) 
    ticks.append(f"{x_ticks[i]:.0f} ({bandwidths[i]:.3f})")

  ax1.set_xticklabels(ticks)

  plt.title(f'Single-Source COBS Data Sink Deltas, pck_len={pck_len}')

  plt.tight_layout()

  plt.show()

plot_stamps(stamps)