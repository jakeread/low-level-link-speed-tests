from cobs_usb_serial_multi import serial_process
from plot_stamps import plot_stamps 
import struct, multiprocessing, time 
import numpy as np 

stamp_count = 1000
pck_len = 128

if __name__ == "__main__":
  ports = ["COM23", "COM31", "COM33", "COM36"]

  queues = [multiprocessing.Queue() for _ in ports]

  processes = [multiprocessing.Process(target=serial_process, args=(port, queue)) for port, queue in zip(ports, queues)]

  stamps = [np.zeros(stamp_count) for _ in ports]
  stamps_lengths = [0 for _ in ports]
  plot_states = [False for _ in ports]

  for p in processes:
    p.start()

  try:
    # infinite loop over process-comm objects; 
    while True:
      for q, queue in enumerate(queues):
        if not queue.empty():
          data = queue.get()
          if len(data) == pck_len:
            if(stamps_lengths[q] >= stamp_count):
              continue
            # unpack each as an integer stamp and store in stamps 
            stamp = struct.unpack("=I", data[:4])[0]
            stamps[q][stamps_lengths[q]] = stamp
            stamps_lengths[q] += 1
            # plot, if the list is done: 
            if stamps_lengths[q] >= stamp_count and not plot_states[q]:
              plot_states[q] = True
              plot_stamps(stamps[q], stamp_count, pck_len)
      
      # to let the processor chill for 1us 
      time.sleep(0.000001)
  except KeyboardInterrupt:
    print("halting...")
  
  for p in processes:
    p.terminate()
    p.join()
