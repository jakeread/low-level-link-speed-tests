## 2023 12 29

So, spinning this up is pretty simple (on the pi 4) we use the configuration tool (UI version actually shows the options we want, command line version misses one) to set UART and SPI interfaces available, then the test code is just using pyserial:

```python
import serial, time 

baud = 2000000

print(f'bit time should be {1000000/baud}us')

ser = serial.Serial('/dev/serial0', baud)

for i in range(10000):
    ser.write(bytearray([95]))
    time.sleep(0.001)

ser.close() 
```

Now, I'm trying to figure where the limit is, so, baud values and measured bit times:

| Baud Setting | Expected Bit Time | Measured | Match |
| --- | --- | --- | --- | 
| 115200 | 8.681us | 8.703us | 0.997 |
| 1000000 | 1.0us | 1.024us | 0.977 |
| 2000000 | 500ns | 512ns | 0.977 |
| 3000000 | 333ns | 352ns | 0.946 |
| 4000000 | 250ns | 271ns | 0.922 |
| 5000000 | 200ns | 223ns | 0.923 |
| 6000000 | 166ns | 176ns | 0.943 |
| 10000000 | 100ns | 111ns | 0.901 |

So we can actually get up to ~ 5MBit/sec and (nearly) survive, though baud settings might need to be trimmed on a scope to get-robust. This is a pretty good sign, and it means we can probably make a serviceable link using UART, though we might end up wanting to deploy two. I'll see quickly about the SPI speeds and then try to figure out where to push. 