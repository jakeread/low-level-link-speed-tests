import serial, time 

baud = 10000000

print(f'bit time should be {1000000/baud}us')

ser = serial.Serial('/dev/serial0', baud)

for i in range(10000):
    ser.write(bytearray([95]))
    time.sleep(0.001)

ser.close() 